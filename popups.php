<?php
    include('header.php');
    include('queries.php');
    include('db_connections.php');
    
    $ms_conn = new db();
    $queries = new queries();
    $popups = $ms_conn->make_query($queries->get_popups());
    unset($db);
    $current_date = date("d/m/Y");
    $last_week_date = date("d/m/Y", strtotime('-1 week'));
?>
    <div class="container">

      <!-- Form Name -->
      <legend>Consulta Ventas de PopUp</legend>
      <!-- Text input-->
      <div class="form-group row"> 
        <div class="col-sm-4">
<!--            <input id="textinput" name="textinput" type="text" placeholder="Introduce el número de pedido" class="form-control input-md" required="">-->
            <select id="numOrder" name="numOrder" class="form-control selectpicker" data-live-search="true" title="Marca..." multiple>
                <?php foreach($popups as $popup) { ?>
                    <option value="<?php echo $popup->DocNum ?>"><?php echo $popup->FirmName . ' (' . date('d/m/Y', strtotime($popup->CreateDate)) . ')' ?></option> 
                <?php } ?>
            </select>
            <button name="numOrder" class="btn clean-select"></button>
        </div>
        <div class="col-sm-4">
            <input id="datepicker" class="form-control" name="date_ini" value="<?php echo $last_week_date ?>" readonly="readonly" type="text">
        </div>
        <div class="col-sm-4">
            <input id="datepicker_end" class="form-control" name="date_end" value="<?php echo $current_date ?>" readonly="readonly" type="text">
        </div>
      </div>

    <!-- Button -->
    <center>
    	<button id="searchbutton" name="searchbutton" class="btn btn-primary">Buscar Ventas</button>
   	</center>
    <br><br><br>
    <div id="resumen"></div>
    <div id="accordion">
      <div class="card">
        <button class="btn btn-link card_head" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <div class="card-header" id="headingOne">
               <h5 class="mb-0">
                  SELLOUT
               </h5>
            </div>
        </button>
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            <div id="sellout"></div>
          </div>
        </div>
      </div>
      <div class="card">
        <button class="btn btn-link card_head collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
             <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                   VENTAS
                </h5>
            </div>
        </button>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
          <div class="card-body">
            <div id="result"></div>
          </div>
        </div>
      </div>
<!--       <div class="card"> -->
<!--         <button class="btn btn-link card_head collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> -->
<!--             <div class="card-header" id="headingThree"> -->
<!--               	<h5 class="mb-0"> -->
<!--                   RESUMEN -->
<!--               	</h5> -->
<!--             </div> -->
<!--         </button> -->
<!--         <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion"> -->
<!--           <div class="card-body"> -->
            
<!--           </div> -->
<!--         </div> -->
<!--       </div> -->
    </div>