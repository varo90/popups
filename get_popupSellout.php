<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    require('db_connections.php');
    require('queries.php');
    
    $orders = $_GET['NumOrder'];
    //$order = '18000392';
    $date_ini = date_format(date_create_from_format('d/m/Y', $_GET['dateIni']), 'Y-m-d');
    $date_end = date_format(date_create_from_format('d/m/Y', $_GET['dateEnd']), 'Y-m-d');
    
    $ms_conn = new db();
    $query = new queries();
    
    $sales = $ms_conn->make_query($query->get_sellout($orders),array($date_ini,$date_end));
    if ($ms_conn->query->rowCount()>0) {
    ?>
        <a id="infoSellout_Export" onclick="exportar('sellout')" class="btn btn-success btn-sm active export" role="button" aria-pressed="true">Exportar a Excel</a>
        <table id="infoSellout" class="table table-striped table-bordered table-condensed" align="center">
        <tr><th>Imagen</th><th>Referencia</th><th>Modelo</th><th>Prov. Catalogo</th><th>Prov. Material</th><th>Color</th><th>Prov. Color</th><th>Un. Compradas</th><th>Un. Vendidas</th><th>Sellout %</th><th>Unidades restantes</th></tr>
        <?php
        foreach($sales as $sale)
        {
            $img = "../../../Fotos_Sap/$sale->img";
            ?>
            <tr>
                <?php //foreach($sale as $col) { ?>
                    <td><a href="<?php echo $img ?>" target="_blank"><img src="<?php echo $img ?>" style="max-width:inherit;width: 60px;"></a></td>
                    <td><?php echo $sale->reference ?></td>
                    <td><?php echo $sale->description ?></td>
                    <td><?php echo $sale->provcat ?></td>
                    <td><?php echo $sale->provmat ?></td>
                    <td><?php echo $sale->color ?></td>
                    <td><?php echo $sale->codes ?></td>
                    <td><?php echo round($sale->bought) ?></td>
                    <td><?php echo round($sale->sold) ?></td>
                    <td><?php echo round($sale->sellout,2) ?></td>
                    <td><?php echo round($sale->unitsRest) ?></td>
                <?php //} ?>
            </tr>
            <?php
        }
        ?>
        </table>
        <?php
    }
    else echo "No hay ventas en este periodo";
    
    unset($ms_conn);
    unset($query);
