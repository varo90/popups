<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    require('db_connections.php');
    require('queries.php');
   
    $orders = $_GET['NumOrder'];
    //$order = '18000392';
    $date_ini = date_format(date_create_from_format('d/m/Y', $_GET['dateIni']), 'Y-m-d');
    $date_end = date_format(date_create_from_format('d/m/Y', $_GET['dateEnd']), 'Y-m-d');

    $ms_conn = new db();
    $query = new queries();
    
    $sales = $ms_conn->make_query($query->get_resum($orders),array($date_ini,$date_end));

    if ($ms_conn->query->rowCount()>0) {
        ?>
        <table id="resum" class="table table-striped table-bordered table-condensed" align="center"><tr><th>Vendedor/a</th><th>Unidades</th><th>Importe</th></tr>
        <?php
        $total_units = 0;
        $total_sales = 0;
        foreach($sales as $sale)
        {
            $total_units += $sale->UNITS;
            $total_sales += $sale->PRICE;
            ?>
            <tr>
                <?php //foreach($sale as $col) { ?>
                <td><?php echo $sale->U_GSP_SESLPNAME ?></td>
                <td><?php echo round($sale->UNITS) ?></td>
                <td><?php echo number_format($sale->PRICE,2,',','.').'€' ?></td>
                <?php //} ?>
            </tr>
            <?php
        }
        ?>
            <tr>
                <td><b><?php echo 'TOTAL' ?></b></td>
                <td><b><?php echo $total_units ?></b></td>
                <td><b><?php echo number_format($total_sales,2,',','.').'€' ?></b></td>
            </tr>
        </table>
        <?php
    }
    else echo "No hay ventas en este periodo";
    
    unset($ms_conn);
    unset($query);
