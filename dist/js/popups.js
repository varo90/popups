$(document).ready(function() {
    $("#datepicker").datepicker();
    $("#datepicker_end").datepicker();
    
    $("#textinput").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $("#searchbutton").click();
        }
    });
      
    $('#searchbutton').on('click',function (e) {
      var reference = $("#numOrder").val();
      var date_ini = $("#datepicker").val();
      var date_end = $("#datepicker_end").val();
      get_sales(reference,date_ini,date_end);
    });
    
    $(".clean-select").on('click', limpiar_select);
});

//$('#infoQuery_Export').on('click',exportar);
function exportar(file) {
    var order = $("#numOrder").val();
    var brand = $("#numOrder :selected").text();
    var date_ini = $("#datepicker").val();
    var date_end = $("#datepicker_end").val();
    $.ajax({
        url: 'export_csv_'+file+'.php',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: {brands:brand,orders:order,dateIni:date_ini,dateEnd:date_end},
        success: function( file ){
            $(location).attr('href',file);
        }
    });
}

function get_sales(reference,date_ini,date_end) {
    if(reference == 0) {
        var msg = '<h4><img src="dist/images/busy.gif" /> Obteniendo ventas...</h4>';
    } else {
        var msg = '<h4><img src="dist/images/busy.gif" /> Obteniendo ventas del pedido '+reference+'</h4>';
    }
    $.blockUI({ message: msg,baseZ: 10000 });
    $.get("get_popupResum.php", {NumOrder:reference,dateIni:date_ini,dateEnd:date_end},function(data,status){
        $('#resumen').html(data);
    });
    $.get("get_popupSales.php", {NumOrder:reference,dateIni:date_ini,dateEnd:date_end},function(data,status){ 
        $('#result').html(data);
    });
    $.get("get_popupSellout.php", {NumOrder:reference,dateIni:date_ini,dateEnd:date_end},function(data,status){
        $.unblockUI();  
        $('#sellout').html(data);
    });
}

function limpiar_select() {
    var id = '#' + $(this).attr('name');
    $( id ).val('default');
    $( id ).selectpicker('refresh');
}

$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    maxDate: 'Today'
};
$.datepicker.setDefaults($.datepicker.regional['es']);