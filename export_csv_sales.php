<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    require('db_connections.php');
    require('queries.php');
    
    $orders = $_POST['orders'];
    $brand = explode(' (',$_POST['brands'])[0];
    $date_ini = date_format(date_create_from_format('d/m/Y', $_POST['dateIni']), 'Y-m-d');
    $date_end = date_format(date_create_from_format('d/m/Y', $_POST['dateEnd']), 'Y-m-d');
    
    $ms_conn = new db();
    $query = new queries();
    
    $sales = $ms_conn->make_query($query->get_sales_export($orders),array($date_ini,$date_end));
    
    if ($ms_conn->query->rowCount()>0) {
        $total_uds = 0;
        $total_price = 0;
        
        $nom_fich = 'popup_'.$brand.'_sales.csv';
        $route_file = 'downloaded/'.$nom_fich;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$nom_fich);
        
        $fp = fopen($route_file, 'w');
        fputcsv($fp, array('Fecha','Vendedor','Transaccion','Cliente','Referencia','Artículo','Precio','Uds.','Total'),';');
        foreach($sales as $sale)
        {
            $total_uds += $sale->quantity;
            $total_price += $sale->price_total;
            $sale->price = round($sale->price,2);
            $sale->quantity = round($sale->quantity);
            //$sale['Quantity'] = round($sale['Quantity']);
            $sale->price_total = round($sale->price_total,2);
            $sale = (array)$sale;
            fputcsv($fp, $sale,';');
        }
        fputcsv($fp, array('','','','','','','',round($total_uds,2),round($total_price,2)),';');
        fclose($fp);
    }
    
    echo $route_file;
    
    unset($ms_conn);
    unset($ms_query);