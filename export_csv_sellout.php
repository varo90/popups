<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    require('db_connections.php');
    require('queries.php');
    
    $orders = $_POST['orders'];
    $brand = explode(' (',$_POST['brands'])[0];
    $date_ini = date_format(date_create_from_format('d/m/Y', $_POST['dateIni']), 'Y-m-d');
    $date_end = date_format(date_create_from_format('d/m/Y', $_POST['dateEnd']), 'Y-m-d');
    
    $ms_conn = new db();
    $query = new queries();
    
    $sellouts = $ms_conn->make_query($query->get_sellout_export($orders),array($date_ini,$date_end));
    
    if ($ms_conn->query->rowCount()>0) {
        $total_bought = 0;
        $total_sold = 0;
        
        $nom_fich = 'popup_'.$brand.'_sellout.csv';
        $route_file = 'downloaded/'.$nom_fich;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$nom_fich);
        
        $fp = fopen($route_file, 'w');
        fputcsv($fp, array('Referencia','Modelo','Prov. Catalogo','Prov. Material','Color','Prov. Color','Un. Compradas','Un. Vendidas','Sellout %','Uds. Restantes'),';');
        foreach($sellouts as $sellout)
        {
            $total_bought += $sellout->bought;
            $total_sold += $sellout->sold;
            $sellout->description = utf8_decode($sellout->description);
            $sellout->bought = number_format($sellout->bought, 2, ',', '.');
            $sellout->sold = round($sellout->sold);
            $sellout->sellout = number_format($sellout->sellout, 2, ',', '.');
            $sellout->unitsRest = round($sellout->unitsRest);
            $sellout = (array)$sellout;
            fputcsv($fp, $sellout,';');
        }
        fputcsv($fp, array('','','','','','',number_format($total_bought, 2, ',', '.'),number_format($total_sold, 2, ',', '.'),number_format(($total_sold/$total_bought)*100, 2, ',', '.'),number_format(($total_bought-$total_sold), 2, ',', '.')),';');
        fclose($fp);
    }
    
    echo $route_file;
    
    unset($ms_conn);
    unset($ms_query);