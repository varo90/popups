<?php

class queries {
    function get_popups() {
        $sql = "SELECT op.DocNum,po.FirmName,op.CreateDate
                FROM OPOR op
                    JOIN (SELECT DISTINCT pos.DocEntry, om.FirmName
                        FROM POR1 pos WITH (NOLOCK)
                        LEFT JOIN OITM oi WITH (NOLOCK) on pos.U_GSP_STAREFERENCE=oi.U_GSP_REFERENCE
                        LEFT JOIN OMRC om WITH (NOLOCK) on om.FirmCode=oi.FirmCode) po ON op.DocEntry=po.DocEntry
                WHERE TrnspCode='7'
                ORDER BY op.DocNum DESC";
        return $sql;
    }
    
    function get_resum($orders) {
        $docsConditions = $this->join_conditions($orders,'op.DocNum');
        $sql = "SELECT tl.U_GSP_SESLPNAME,SUM(tl.U_GSP_LIQUAN) AS UNITS,SUM(tl.U_GSP_LIIMPR) AS PRICE
                FROM [dbo].[@GSP_TPVLIN] tl WITH (NOLOCK)
                    RIGHT JOIN (SELECT Distinct ItemCode FROM SBO_EULALIA.dbo.POR1 as po WITH (NOLOCK) 
                                LEFT JOIN opor op WITH (NOLOCK) on op.DocEntry=po.DocEntry
                                WHERE ($docsConditions)) as po on po.ItemCode=tl.U_GSP_LIARTI
                WHERE tl.U_GSP_LIDATATPV BETWEEN ? AND ?
                GROUP BY tl.U_GSP_SESLPNAME
                ORDER BY SUM(tl.U_GSP_LIIMPR) DESC";
        return $sql;
    }
    
    function get_sales($orders) {
        $docsConditions = $this->join_conditions($orders,'op.DocNum');
        $sql = "SELECT oi.PicturName AS picture, CONCAT(CONVERT(VARCHAR(10), FORMAT(tl.U_GSP_LIDATATPV, 'dd-MM-yyyy'), 105),' ',tl.U_GSP_LIHORATPV) as date, tl.U_GSP_SESLPNAME AS seller,tl.U_GSP_LINUME AS transact,tc.U_GSP_CACLNOM AS client,oi.U_GSP_REFERENCE AS reference,oi.ItemName AS description,tl.U_GSP_LIPREU AS price,tl.U_GSP_LIQUAN AS quantity,tl.U_GSP_LIIMPR AS price_total
                  FROM POR1 po JOIN [dbo].[@GSP_TPVLIN] tl WITH (NOLOCK) on tl.U_GSP_LIARTI=po.ItemCode LEFT JOIN opor op WITH (NOLOCK) on op.DocEntry=po.DocEntry LEFT JOIN [dbo].[@GSP_TPVCAP] tc WITH (NOLOCK) on tc.U_GSP_CANUME=tl.U_GSP_LINUME LEFT JOIN OITM oi WITH (NOLOCK) ON po.ItemCode=oi.ItemCode
                  WHERE ($docsConditions) AND tl.U_GSP_LIDATATPV BETWEEN ? AND ?
                  GROUP BY oi.PicturName, CONCAT(CONVERT(VARCHAR(10), FORMAT(tl.U_GSP_LIDATATPV, 'dd-MM-yyyy'), 105),' ',tl.U_GSP_LIHORATPV), tl.U_GSP_SESLPNAME,tl.U_GSP_LINUME,tc.U_GSP_CACLNOM,oi.U_GSP_REFERENCE,oi.ItemName,tl.U_GSP_LIPREU,tl.U_GSP_LIQUAN,tl.U_GSP_LIIMPR
                  ORDER BY MIN(tl.U_GSP_LIDATATPV) desc, MIN(tl.U_GSP_LIHORATPV) desc";
        return $sql;
    }
    
    function get_sellout($orders) {
        $docsConditions = $this->join_conditions($orders,'op.DocNum');
        $sql="SELECT oi.PicturName as img,
        oi.U_GSP_REFERENCE as reference,
        mo.U_GSP_Desc as description,
        tcmca.U_GSP_CATALOGNUM AS provcat,
        COALESCE(tcmca.U_GSP_MATERIALDESC,'') AS provmat,
        col.U_GSP_Name as color,
        COALESCE(tcmca.U_GSP_COLORDESC,'') as codes,
        po.Quantity as bought,
        SUM(tl.U_GSP_LIQUAN) as sold,
        (SUM(tl.U_GSP_LIQUAN)/po.Quantity)*100 as sellout,
        (po.Quantity-(SUM(tl.U_GSP_LIQUAN))) AS unitsRest,
        SUM(tl.U_GSP_LIIMPR) as moneysold
        FROM [dbo].[@GSP_TPVLIN] tl with (NOLOCK)
            LEFT JOIN OITM oi with (NOLOCK) ON tl.U_GSP_LIARTI=oi.ItemCode
            RIGHT JOIN (SELECT U_GSP_STAREFERENCE, oi.U_GSP_Color ,SUM(Quantity) AS Quantity FROM SBO_EULALIA.dbo.POR1 as po WITH (NOLOCK)
                        LEFT JOIN opor op WITH (NOLOCK) on op.DocEntry=po.DocEntry
                        LEFT JOIN oitm oi WITH (NOLOCK) on oi.ItemCode=po.ItemCode
                        WHERE ($docsConditions)
                        GROUP BY U_GSP_STAREFERENCE,oi.U_GSP_Color) as po on po.U_GSP_STAREFERENCE=oi.U_GSP_REFERENCE and po.U_GSP_Color=oi.U_GSP_Color
            LEFT JOIN [dbo].[@GSP_TCMODEL] mo with (NOLOCK) ON oi.U_GSP_ModelCode=mo.Code
            LEFT JOIN [dbo].[@GSP_TCCOLOR] col with (NOLOCK) ON oi.U_GSP_Color=col.Code
            LEFT JOIN [dbo].[@GSP_TCMODELCATALOG] tcmca with (NOLOCK) ON oi.U_GSP_MODELCODE = tcmca.U_GSP_MODELCODE and tcmca.U_GSP_MODELCOLOR = col.Code
        WHERE tl.U_GSP_LIDATATPV BETWEEN ? AND ?
        GROUP BY oi.PicturName,oi.U_GSP_REFERENCE,mo.U_GSP_Desc,tcmca.U_GSP_CATALOGNUM,COALESCE(tcmca.U_GSP_MATERIALDESC,''),col.U_GSP_Name,COALESCE(tcmca.U_GSP_COLORDESC,''),po.Quantity";
        return $sql;
    }
    
    function get_sales_export($orders) {
        $docsConditions = $this->join_conditions($orders,'op.DocNum');
        $sql="SELECT CONCAT(CONVERT(VARCHAR(10), FORMAT(tl.U_GSP_LIDATATPV, 'dd-MM-yyyy'), 105),' ',tl.U_GSP_LIHORATPV) as date, tl.U_GSP_SESLPNAME AS seller,tl.U_GSP_LINUME AS transact,tc.U_GSP_CACLNOM AS client,oi.U_GSP_REFERENCE AS reference,oi.ItemName AS description,tl.U_GSP_LIPREU AS price,tl.U_GSP_LIQUAN AS quantity,tl.U_GSP_LIIMPR AS price_total
                FROM POR1 po WITH (NOLOCK) JOIN [dbo].[@GSP_TPVLIN] tl WITH (NOLOCK) on tl.U_GSP_LIARTI=po.ItemCode LEFT JOIN opor op WITH (NOLOCK) on op.DocEntry=po.DocEntry LEFT JOIN [dbo].[@GSP_TPVCAP] tc WITH (NOLOCK) on tc.U_GSP_CANUME=tl.U_GSP_LINUME LEFT JOIN OITM oi WITH (NOLOCK) ON po.ItemCode=oi.ItemCode
                WHERE ($docsConditions) AND tl.U_GSP_LIDATATPV BETWEEN ? AND ?
                GROUP BY CONCAT(CONVERT(VARCHAR(10), FORMAT(tl.U_GSP_LIDATATPV, 'dd-MM-yyyy'), 105),' ',tl.U_GSP_LIHORATPV), tl.U_GSP_SESLPNAME,tl.U_GSP_LINUME,tc.U_GSP_CACLNOM,oi.U_GSP_REFERENCE,oi.ItemName,tl.U_GSP_LIPREU,tl.U_GSP_LIQUAN,tl.U_GSP_LIIMPR
                ORDER BY MIN(tl.U_GSP_LIDATATPV) desc, MIN(tl.U_GSP_LIHORATPV) desc";
        return $sql;
    }
    
    function get_sellout_export($orders) {
        $docsConditions = $this->join_conditions($orders,'op.DocNum');
        $sql = "SELECT oi.U_GSP_REFERENCE as reference,
        mo.U_GSP_Desc as description,
        tcmca.U_GSP_CATALOGNUM AS provcat,
        COALESCE(tcmca.U_GSP_MATERIALDESC,'') AS provmat,
        col.U_GSP_Name as color,
        COALESCE(tcmca.U_GSP_COLORDESC,'') as codes,
        po.Quantity as bought,
        SUM(tl.U_GSP_LIQUAN) as sold,
        (SUM(tl.U_GSP_LIQUAN)/po.Quantity)*100 as sellout,
        (po.Quantity-(SUM(tl.U_GSP_LIQUAN))) AS unitsRest
        FROM [dbo].[@GSP_TPVLIN] tl with (NOLOCK)
            LEFT JOIN OITM oi with (NOLOCK) ON tl.U_GSP_LIARTI=oi.ItemCode
            RIGHT JOIN (SELECT U_GSP_STAREFERENCE, oi.U_GSP_Color ,SUM(Quantity) AS Quantity FROM SBO_EULALIA.dbo.POR1 as po WITH (NOLOCK)
                        LEFT JOIN opor op WITH (NOLOCK) on op.DocEntry=po.DocEntry
                        LEFT JOIN oitm oi WITH (NOLOCK) on oi.ItemCode=po.ItemCode
                        WHERE ($docsConditions)
                        GROUP BY U_GSP_STAREFERENCE,oi.U_GSP_Color) as po on po.U_GSP_STAREFERENCE=oi.U_GSP_REFERENCE and po.U_GSP_Color=oi.U_GSP_Color
            LEFT JOIN [dbo].[@GSP_TCMODEL] mo with (NOLOCK) ON oi.U_GSP_ModelCode=mo.Code
            LEFT JOIN [dbo].[@GSP_TCCOLOR] col with (NOLOCK) ON oi.U_GSP_Color=col.Code
            LEFT JOIN [dbo].[@GSP_TCMODELCATALOG] tcmca with (NOLOCK) ON oi.U_GSP_MODELCODE = tcmca.U_GSP_MODELCODE and tcmca.U_GSP_MODELCOLOR = col.Code
        WHERE tl.U_GSP_LIDATATPV BETWEEN ? AND ?
        GROUP BY oi.U_GSP_REFERENCE,mo.U_GSP_Desc,tcmca.U_GSP_CATALOGNUM,COALESCE(tcmca.U_GSP_MATERIALDESC,''),col.U_GSP_Name,COALESCE(tcmca.U_GSP_COLORDESC,''),po.Quantity";
        return $sql;
    }
    
    function join_conditions($filters,$sql) {
        $texts = array();
        foreach($filters as $filter) {
            $texts[] = "$sql='$filter'";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
}
