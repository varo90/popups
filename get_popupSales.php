<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    require('db_connections.php');
    require('queries.php');
   
    $orders = $_GET['NumOrder'];
    //$order = '18000392';
    $date_ini = date_format(date_create_from_format('d/m/Y', $_GET['dateIni']), 'Y-m-d');
    $date_end = date_format(date_create_from_format('d/m/Y', $_GET['dateEnd']), 'Y-m-d');
    
    $ms_conn = new db();
    $query = new queries();
    
    $sales = $ms_conn->make_query($query->get_sales($orders),array($date_ini,$date_end));
    
    if ($ms_conn->query->rowCount()>0) {
        ?>
        <a id="infoQuery_Export" onclick="exportar('sales')" class="btn btn-success btn-sm active export" role="button" aria-pressed="true">Exportar a Excel</a>
        <table id="infoQuery" class="table table-striped table-bordered table-condensed" align="center"><tr><th>Imagen</th><th>Fecha</th><th>Vendedor/a</th><th>Transacci&oacute;n</th><th>Cliente</th><th>Referencia</th><th>Art&iacute;culo</th><th>Precio</th><th>Uds.</th><th>Total</th></tr>
        <?php
        foreach($sales as $sale)
        {
            $img = "../../../Fotos_Sap/$sale->picture";
            ?>
            <tr>
                <?php //foreach($sale as $col) { ?>
                    <td><a href="<?php echo $img ?>" target="_blank"><img src="<?php echo $img ?>" style="max-width:inherit;width: 60px;"></a></td>
                    <td><?php echo date('d/m/Y H:i:s', strtotime($sale->date)); ?></td>
                    <td><?php echo $sale->seller ?></td>
                    <td><?php echo $sale->transact ?></td>
                    <td><?php echo $sale->client ?></td>
                    <td><?php echo $sale->reference ?></td>
                    <td><?php echo $sale->description ?></td>
                    <td><?php echo number_format($sale->price, 2, ',', '.').'€' ?></td>
                    <td><?php echo round($sale->quantity) ?></td>
                    <td><?php echo number_format($sale->price_total, 2, ',', '.').'€' ?></td>
                <?php //} ?>
            </tr>
            <?php
        }
        ?>
        </table>
        <?php
    }
    else echo "No hay ventas en este periodo";
    
    unset($ms_conn);
    unset($query);
