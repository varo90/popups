<?php

    include('db_connections.php');
    include('queries.php');
    include('exportXML.php');
    
    $db = new db();
    $model = new queries();
    $xml = new exportXML();
    
    $vales = $db->make_query($model->get_vales());
    unset($db);
    
    $xml_info = $xml->generate_xml($vales);
    
    generate_file($xml_info);

    function generate_file($xml_info) {
        $fichero = 'downloaded/vales.xml';
        $myfile = fopen($fichero, 'w') or die('Unable to open file!');
        file_put_contents($fichero, $xml_info);
        fclose($myfile);
    }

    